
// If your program is a part of some package create the package directory first
package demo;

// Anything you intend to use from other packages has to be imported
import java.util.*;

/** Demo class to explain the class structure. File name of
 * the compilation unit has to be "Example.java"
 */
public class Example {

   /** class variables are "global" and common for all instances */
   public static int maxSize = 10000;

   /** instance variables have individual value for each object */
   private int size = 0; // data hiding - make fields private

   /** constructors are specific class methods to create an object */
   Example (int i) {
      size = i; // equal to "this.size = i;"
   }

   /** constructors may have different signatures (compile time overloading) */
   Example() {
      this (10); // call another constructor that has different signature
   }

   /** class methods are "static" (do not depend on objects) */
   public static Example getInstance() {
      return new Example();
   }

   /** instance methods run on "this" object (receiver object) */
   public int size() {
      return size;
   }

   /** Conversion to string.
    * @return string representation of the object
    */
   @Override  // indicates run-time overriding
   public String toString() {
      return String.valueOf (size()); // this.size()
   }

   /** Java applications start from the main-method.
    * @param args command line parameters for the program
    */
   public static void main (String[] args) {
      Example e1 = new Example (5); // creating an object with constructor
      Example e2 = getInstance();   // using class method Example.getInstance()
      System.out.println (String.valueOf (Example.maxSize)); // class variable
      System.out.println (e1);      // indirect call of toString()
      int s2 = e2.size();           // sending "size" message to the object e2
   } // end of main

} // end of Example

